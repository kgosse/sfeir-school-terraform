variable "application_name" {
  type        = string    
  description = "The name of the instance."
  default     = "demo-user-kgosse-production"    
}
variable "machine_type" {
  type        = string
  description = "The machine type"
  default     = "n1-standard-1"    
}
variable "gcp_project" {
  type        = string
  description = "The GCP project ID"
  default = "sfeir-school-terraform"    
}
